

//#######################
//##########  Show case ##########
//#####################


$(document).ready(function(){
    
    
//#######################
//##########  Slider ##########
//#####################  
  
//  window.onload=function(){
  $('.slider').slick({
  autoplay:true,
  autoplaySpeed:6000,
  arrows:true,
    dots:true,
//   fade: true,
  prevArrow:'<button type="button" class="slick-prev"></button>',
  nextArrow:'<button type="button" class="slick-next"></button>',
  centerMode:true,
  slidesToShow:1,
  slidesToScroll:1
  });
      
      
    
      
      
//};
  
    
//##########  Slider ##########  
    
    
    
    
// Masonry as a function
function maso(){
  
$('.grid').masonry({
    itemSelector: '.element-item',
    columnWidth: '.element-item',
    percentPosition: true
  });

 }



// init Isotope
var $grid = $('.grid').isotope({
  itemSelector: '.element-item',
  layoutMode: 'fitRows'
});

    
// bind filter button click
$('.filters-button-group').on( 'click', 'button', function() {
  var filterValue = $( this ).attr('data-filter').toLowerCase();
  $grid.isotope({ filter: filterValue }); 
});


// call on load
maso();


    
//##########  Show case ##########
   
    
    
    
  
    

    
//################################    
//#################  MENU ################ 
//############################
    
 
    
$(".burger").on("click", function(){
    
    
    $("nav").animate({"margin-left":"0"});
    
    $(".open-ico").hide(300);
    $(".close-ico").show(400);
    $(".menu-list").animate({"max-width":"100%"});
    
    
});    
    
    
    
$(".close-ico").on("click", function(){
    
    $(".menu-list").animate({"max-width":"200px"});
    $("nav").animate({"margin-left":"-200px"});
    
    $(".open-ico").show(400);
    $(".close-ico").hide(300);
    
    
    
});     
    
    
$(".menu-list>li").on("click", function(){
    
    var $this = $(this);
    $(".menu-list>li>ul").hide(600);
    $this.children().toggle(400);
    
    
    
});         
    
    
    
    
    
    
    
//################### END MENU ####################    
    
    
    

  
  
  })